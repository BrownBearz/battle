import java.io.*;
import java.util.Random;
import static java.lang.System.*;
import javax.swing.JOptionPane;

public class BattleOld
{
	private static int hp, atk, def;
	private static int hp2, atk2, def2, num,num2;
	private static int p1Win,p2Win,draw;
	private static String name1, name2;
	private static int tempDef, tempDef2, p1Special,p2Special;
	private static int tempHp, tempHp2, potionNum, potionNum2;
	private static Object p1type,p2type,p1choice, p2choice;
	private static Object choice2, choice3;
	private static Object p1Type, p2Type;
	private static Object confirm, sType;
	
	public static void main(String args[]) throws IOException
	{
		out.println("            *******************************************************");
		out.println("            *******************************************************");
		out.println("            **                                                   **");
		out.println("            **  BBBBB      !     TTTTTTT  TTTTTTT  LL     EEEEE  **");
		out.println("            **  B   BB    ! !      TTT      TTT    LL     EE     **");
		out.println("            **  BBBBBB   !!!!!     TTT      TTT    LL     EEEEE  **");
		out.println("            **  B   BB  !     !    TTT      TTT    LL     EE     **");
		out.println("            **  BBBBB   !     !    TTT      TTT    LLLLL  EEEEE  **");
		out.println("            **                                                   **");
		out.println("            *******************************************************");
		out.println("            *******************************************************");
		out.println();
		out.println("           Copyright (c) Newman's Best. All Rights Reserved for Battle");
		out.println();
		out.println("                    Created by Travis *Freakin Awesome* Sims       ");
		out.println("             Assisted by Ryan *Sleeps and Gains Knowledge* Artecona");
		out.println();
		
		BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
		potionNum=1;
		potionNum2=1;
		String name1=JOptionPane.showInputDialog(null,"Enter Player 1's Name","Player 1 Set-up!",JOptionPane.INFORMATION_MESSAGE);
		Object[] sType={"Male","Female"};
		p1Type=JOptionPane.showInputDialog(null,name1+"...what is your gender?","Player 1 Set-up!",JOptionPane.INFORMATION_MESSAGE,null,sType,sType[0]);
		Object[] charType={"Warrior","Knight","Tank"};
		p1type=JOptionPane.showInputDialog(null,name1+"...choose your character class","Player 1 Set-up!",JOptionPane.INFORMATION_MESSAGE,null,charType,charType[0]);
		characterClass(charType,p1type);
		Object[] weapType={"Blunt","Slashing"};
		p1choice=JOptionPane.showInputDialog(null,name1+"...which weapon type will you use?","Choose Your Weapon!",JOptionPane.INFORMATION_MESSAGE,null,weapType,weapType[0]);
		if(p1choice.equals("Blunt"))
		{
			Object[] bluntType={"Morning Star","Club","Flail"};
			choice2=JOptionPane.showInputDialog(null,"Which Blunt type will you use?","Choose Your Weapon!",JOptionPane.INFORMATION_MESSAGE,null,bluntType,bluntType[0]);
			bluntClass(bluntType, choice2);
		}
		if(p1choice.equals("Slashing"))
		{
			Object[] slashType={"Dagger","Scythe","Bastard Sword"};
			choice2=JOptionPane.showInputDialog(null,"Which Slashing type will you use?","Choose Your Weapon!",JOptionPane.INFORMATION_MESSAGE,null,slashType,slashType[0]);
			slashClass(slashType, choice2);
		}
		if(p1choice.equals("Blunt"))
			JOptionPane.showMessageDialog(null,"Your stats for the Battle will be:\n\nName:         "+name1+"\nGender:       "+p1Type+"\nClass:          "+p1type+"\nWeapon:     "+choice2+" (Blt)\nHP:               "+hp+"\nAttack:        "+atk+"\nDefense:     "+def,"Confirm Stats!",JOptionPane.INFORMATION_MESSAGE);
		if(p1choice.equals("Slashing"))
			JOptionPane.showMessageDialog(null,"Your stats for the Battle will be:\n\nName:         "+name1+"\nGender:       "+p1Type+"\nClass:          "+p1type+"\nWeapon:     "+choice2+" (Slc)\nHP:               "+hp+"\nAttack:        "+atk+"\nDefense:     "+def,"Confirm Stats!",JOptionPane.INFORMATION_MESSAGE);
		name2=JOptionPane.showInputDialog(null,"Enter Player 2's Name","Player 2 Set-up!",JOptionPane.INFORMATION_MESSAGE);
		p2Type=JOptionPane.showInputDialog(null,name2+"...what is your gender?","Player 2 Set-up!",JOptionPane.INFORMATION_MESSAGE,null,sType,sType[0]);
		p2type=JOptionPane.showInputDialog(null,name2+"...choose your character class","Player 2Set-up!",JOptionPane.INFORMATION_MESSAGE,null,charType,charType[0]);
		characterClass2(charType,p2type);
		
		p2choice=JOptionPane.showInputDialog(null,name2+"...which weapon type will you use?","Choose Your Weapon!",JOptionPane.INFORMATION_MESSAGE,null,weapType,weapType[0]);
		if(p2choice.equals("Blunt"))
		{
			Object[] bluntType={"Morning Star","Club","Flail"};
			choice3=JOptionPane.showInputDialog(null,"Which Blunt type will you use?","Choose Your Weapon!",JOptionPane.INFORMATION_MESSAGE,null,bluntType,bluntType[0]);
			bluntClass2(bluntType, choice3);
		}
		if(p2choice.equals("Slashing"))
		{
			Object[] slashType={"Dagger","Scythe","Bastard Sword"};
			choice3=JOptionPane.showInputDialog(null,"Which Slashing type will you use?","Choose Your Weapon!",JOptionPane.INFORMATION_MESSAGE,null,slashType,slashType[0]);
			slashClass2(slashType, choice3);
		}
		if(p2choice.equals("Blunt"))
			JOptionPane.showMessageDialog(null,"Your stats for the Battle will be:\n\nName:         "+name2+"\nGender:      "+p2Type+"\nClass:          "+p2type+"\nWeapon:     "+choice3+" (Blt)\nHP:               "+hp2+"\nAttack:        "+atk2+"\nDefense:     "+def2,"Confirm Stats!",JOptionPane.INFORMATION_MESSAGE);
		if(p2choice.equals("Slashing"))
			JOptionPane.showMessageDialog(null,"Your stats for the Battle will be:\n\nName:         "+name2+"\nGender:      "+p2Type+"\nClass:          "+p2type+"\nWeapon:     "+choice3+" (Slc)\nHP:               "+hp2+"\nAttack:        "+atk2+"\nDefense:     "+def2,"Confirm Stats!",JOptionPane.INFORMATION_MESSAGE);
		
		int times=Integer.parseInt(JOptionPane.showInputDialog(null,"How many times shall you do battle?","Warm-up!",JOptionPane.INFORMATION_MESSAGE));
		tempHp=hp;
		tempHp2=hp2;
		tempDef=def;
		tempDef2=def2;
		for(int i=1; i<=times; i++)
		{
			hp=tempHp;
			hp2=tempHp2;
			out.println("BATTLE "+i);
			out.println();
			boolean PTurn=true;
			do
			{
				if(PTurn==true)
				{
					hp2=attackP2(name1,name2,hp,hp2,atk,atk2,tempDef,def,def2);
					PTurn=false;
				}
				else
				{
					hp=attackP1(name1,name2,hp,atk2,def,def2,tempDef2,hp2);
					PTurn=true;
				}
			}while(hp>0&&hp2>0);
			if(hp<=0)
			{
				Random rand=new Random();
				int chance=rand.nextInt(11);
				if(chance==7)
				{
					JOptionPane.showMessageDialog(null,name1+" has been slain!","GAME OVER",JOptionPane.INFORMATION_MESSAGE);
					JOptionPane.showMessageDialog(null,"Wait..."+name1+" got up for a last stand and impaled "+name2,"Last Stand!",JOptionPane.INFORMATION_MESSAGE);
					JOptionPane.showMessageDialog(null,name2+" has been slain!","GAME OVER",JOptionPane.INFORMATION_MESSAGE);
					draw++;
				}
				else
				{
					JOptionPane.showMessageDialog(null,name1+" has been slain!","GAME OVER",JOptionPane.INFORMATION_MESSAGE);
					p2Win++;
				}
			}
			if(hp2<=0)
			{
				Random rand2=new Random();
				int chance=rand2.nextInt(11);
				if(chance==7)
				{
					JOptionPane.showMessageDialog(null,name2+" has been slain!","GAME OVER", JOptionPane.INFORMATION_MESSAGE);
					JOptionPane.showMessageDialog(null,"Wait..."+name2+" got up for a last stand and impaled "+name1,"Last Stand!",JOptionPane.INFORMATION_MESSAGE);
					JOptionPane.showMessageDialog(null,name1+" has been slain!","GAME OVER",JOptionPane.INFORMATION_MESSAGE);
					draw++;
				}
				else
				{
					JOptionPane.showMessageDialog(null,name2+" has been slain!","GAME OVER",JOptionPane.INFORMATION_MESSAGE);
					p1Win++;
				}
			}
			JOptionPane.showMessageDialog(null,name1+" won "+p1Win+" times out of "+times+"\n"+name2+" won "+p2Win+" times out of "+times+"\nThere were "+draw+" draws","Results!",JOptionPane.INFORMATION_MESSAGE);
			out.println();

			
		}
		System.exit(0);
	}
	
	public static Object bluntClass(Object[] weapType, Object choice2)
	{
		if(choice2.equals("Morning Star"))
		{
			atk+=5;
			def+=5;
		}
		if(choice2.equals("Club"))
		{
			atk-=5;
			def+=10;
		}
		if(choice2.equals("Flail"))
		{
			atk+=10;
			def-=5;
		}
		return choice2;		
	}
	
	public static Object bluntClass2(Object[] weapType, Object choice3)
	{
		if(choice3.equals("Morning Star"))
		{
			atk2+=5;
			def2+=5;
		}
		if(choice3.equals("Club"))
		{
			atk2-=5;
			def2+=10;
		}
		if(choice3.equals("Flail"))
		{
			atk2+=10;
			def2-=5;
		}
		return choice3;
	}
	
	public static Object slashClass(Object[] weapType, Object choice2)
	{
		if(choice2.equals("Dagger"))
		{
			atk+=5;
			def+=5;
		}
		if(choice2.equals("Scythe"))
		{
			atk-=5;
			def+=10;
		}
		if(choice2.equals("Bastard Sword"))
		{
			atk+=10;
			def-=5;
		}
		return choice2;
	}
	
	public static Object slashClass2(Object[] weapType, Object choice3)
	{
		if(choice3.equals("Dagger"))
		{
			atk2+=5;
			def2+=5;
		}
		if(choice3.equals("Scythe"))
		{
			atk2-=5;
			def2+=10;
		}
		if(choice3.equals("Bastard Sword"))
		{
			atk2+=10;
			def2-=5;
		}
		return choice3;
	}
	
	public static Object characterClass(Object[] charType, Object type)
	{
		if(type.equals("Warrior"))
		{
			hp=300;
			atk=75;
			def=50;
		}
		if(type.equals("Knight"))
		{
			hp=300;
			atk=50;
			def=80;
		}
		if(type.equals("Tank"))
		{
			hp=600;
			atk=60;
			def=45;
		}
		return type;
	}
	
	public static Object characterClass2(Object[] charType, Object type)
	{
		if(type.equals("Warrior"))
		{
			hp2=300;
			atk2=75;
			def2=50;
		}
		if(type.equals("Knight"))
		{
			hp2=300;
			atk2=50;
			def2=80;
		}
		if(type.equals("Tank"))
		{
			hp2=600;
			atk2=60;
			def2=45;
		}
		return type;
	}
	
	public static int attackP2(String name1, String name2,int hp, int hp2, int atk,int atk2,int tempDef, int def,int def2) throws IOException
	{
		def=tempDef;
		Random rand=new Random();
		int damage;
		Object[] possibleValues={"Attack","Defend","Items","Specials"};
		Object choice=JOptionPane.showInputDialog(null,name1+"'s HP:  "+hp+"\nChoose Your Command!?","Choose Your Command!",JOptionPane.INFORMATION_MESSAGE,null,possibleValues,possibleValues[0]);
		if(choice.equals("Attack"))
		{
			damage=atk-def2;
			if(damage>0)
			{
				damage=rand.nextInt(damage);
				if(damage==0)
					out.println(name1+" trips and misses "+name2+"\n");
				else
				{
					if(p1Type.equals("Male"))
					{
						out.println(name1+" attacks "+name2+" with his "+choice2+" for "+damage+" damage!\n");
						hp2-=damage;
					}
					else
					{
						out.println(name1+" attacks "+name2+" with her "+choice2+" for "+damage+" damage!\n");
						hp2-=damage;
					}
				}
			}
			if(damage<0)
			{
				damage=rand.nextInt((int)Math.ceil(atk*.3));
				hp2-=damage;
			}	
		}
		if(choice.equals("Defend"))
		{
			def*=2;
			out.println(name1+" holds up his/her "+choice2+" in a defensive position!\n");
		}
		if(choice.equals("Items"))
			p1Items(name1);
		if(choice.equals("Specials"))
		{
			Object[] specials={"Double-Strike","Go Back"};
			Object sChoice=JOptionPane.showInputDialog(null,name1+"'s HP:  "+hp+"\n"+name1+"'s SP:  "+p1Special+"\nChoose your Special!","Choose Your Command!",JOptionPane.INFORMATION_MESSAGE,null,specials,specials[0]);
			if(sChoice.equals("Go Back"))
				attackP2(name1,name2,hp,hp2,atk,atk2,tempDef,def,def2);
		}
		return hp2;
	}
	public static void p1Items(String name1) throws IOException
	{
		Object items[]={"Potion","Go Back"};
		Object choice2=JOptionPane.showInputDialog(null,name1+"'s HP:  "+hp+"\nWhat item will you use?","Choose Your Command!",JOptionPane.INFORMATION_MESSAGE,null,items, items[0]);
		if(choice2.equals("Go Back"))
			attackP2(name1,name2,hp,hp2,atk,atk2,tempDef,def,def2);
		Random rand3=new Random();
		if(potionNum==0)
		{
			JOptionPane.showMessageDialog(null,"Sorry, no more potions left");
			p1Items(name1);
		}
		else
		{
			int chance2=rand3.nextInt(11);
			if(chance2==7)
			{
				if(hp==tempHp)
				{
					JOptionPane.showMessageDialog(null,"You don't need a potion when you have full HP!","Full HP!",JOptionPane.INFORMATION_MESSAGE);
					attackP2(name1,name2,hp,hp2,atk,atk2,tempDef,def,def2);
					attackP1(name1, name2,hp,atk2,def,def2,tempDef2,hp2);
				}
				else
				{
					num=(tempHp-hp)/2;
					if(hp>=tempHp)
						hp=tempHp;
					if(num>=20)
					{
						out.println(name1+" chokes on his potion and only can consume half of it!\n");
						out.println(name1+" restores 20 HP");
						hp+=20;
					}
					else
					{
						out.println(name1+" chokes on his potion and only can consume half of it!\n");
						out.println(name1+" restores "+num+" HP");
						hp+=num;
					}
					potionNum--;
				}
				
			}
			else
			{
				if(hp==tempHp)
				{
					JOptionPane.showMessageDialog(null,"You don't need a potion when you have full HP!","Full HP!",JOptionPane.INFORMATION_MESSAGE);
					attackP2(name1,name2,hp,hp2,atk,atk2,tempDef,def,def2);
					attackP1(name1, name2,hp,atk2,def,def2,tempDef2,hp2);
				}
				else
				{
					num=tempHp-hp;
					if(hp>=tempHp)
						hp=tempHp;
					if(num>=40)
					{
						out.println(name1+" consumes a potion and restores 40 HP\n");
						hp+=40;
					}
					else
					{
						out.println(name1+" consumes a potion and restores "+num+" HP\n");
						hp+=num;
					}
					potionNum--;

				}
			}
		}
	}
	public static void p2Items(String name2) throws IOException
	{
		Object items[]={"Potion","Go Back"};
		Object choice3=JOptionPane.showInputDialog(null,name2+"'s HP:  "+hp2+"\nWhat item will you use?","Choose Your Command!",JOptionPane.INFORMATION_MESSAGE,null,items, items[0]);
		if(choice3.equals("Go Back"))
			attackP1(name1,name2,hp,atk2,def,def2,tempDef2,hp2);
		Random rand3=new Random();
		if(potionNum2==0)
		{
			JOptionPane.showMessageDialog(null,"Sorry, no more potions left");
			p2Items(name2);
		}
		else
		{
			int chance2=rand3.nextInt(11);
			if(chance2==7)
			{
				if(hp2==tempHp2)
				{
					JOptionPane.showMessageDialog(null,"You don't need a potion when you have full HP!","Full HP!",JOptionPane.INFORMATION_MESSAGE);
					attackP1(name1,name2,hp,atk2,def,def2,tempDef2,hp2);
					attackP2(name1,name2,hp,hp2,atk,atk2,tempDef,def,def2);
				}
				else
				{
					num2=(tempHp2-hp2)/2;
					if(hp2>=tempHp2)
						hp2=tempHp2;
					if(num2>=20)
					{
						out.println(name2+" chokes on his potion and only can consume half of it!\n");
						out.println(name2+" restores 20 HP");
						hp2+=20;
					}
					else
					{
						out.println(name2+" chokes on his potion and only can consume half of it\n");
						out.println(name2+" restores "+num2+" \n");
						hp2+=num2;
					}
					potionNum2--;
					
				}
			}
			else
			{
				if(hp2==tempHp2)
				{
					JOptionPane.showMessageDialog(null,"You don't need a potion when you have full HP!","Full HP!",JOptionPane.INFORMATION_MESSAGE);
					attackP1(name1,name2,hp,atk2,def,def2,tempDef2,hp2);
					attackP2(name1,name2,hp,hp2,atk,atk2,tempDef,def,def2);
				}
				else
				{
					num2=tempHp2-hp2;
					if(hp2>=tempHp2)
						hp2=tempHp2;
					if(num2>=40)
					{
						out.println(name2+" consumes a potion and restores 40 HP\n");
						hp2+=40;
					}
					else 
					{
						out.println(name2+" consumes a potion and restores "+num2+" HP\n");
						hp2+=num2;
					}
					potionNum2--;
				
				}
			}
		}
	}
	public static int attackP1(String name1, String name2,int hp, int atk2,int def,int def2, int tempDef2,int hp2) throws IOException
	{
		def2=tempDef2;
		Random rand=new Random();
		int damage;
		Object[] possibleValues={"Attack","Defend","Items","Specials"};
		Object choice=JOptionPane.showInputDialog(null,name2+"'s HP:  "+hp2+"\nChoose Your Command!","Choose Your Command!",JOptionPane.INFORMATION_MESSAGE,null,possibleValues, possibleValues[0]);
		if(choice.equals("Attack"))
		{
			damage=atk2-def;
			if(damage>0)
			{
				damage=rand.nextInt(damage);
				if(damage==0)
					out.println(name2+" trips and misses "+name1+"\n");
				else
				{
					if(p2Type.equals("Male"))
						out.println(name2+" attacks "+name1+" with his "+choice3+" for "+damage+ " damage!\n");
					else
						out.println(name2+" attacks "+name1+" with her "+choice3+" for "+damage+ " damage!\n");
				}
			}
			if(damage<0)
				damage=rand.nextInt((int)Math.ceil(atk2*.3));
			hp-=damage;
			p2Special+=damage;
		
		}
		if(choice.equals("Defend"))
		{
			def2*=2;
			out.println(name2+" holds up his/her "+choice3+" in a defensive position!\n");
		}
		if(choice.equals("Items"))
			p2Items(name2);
		if(choice.equals("Specials"))
		{
			Object[] special2={"Double-Strike","Go Back"};
			Object sChoice2=JOptionPane.showInputDialog(null,name2+"'s HP:  "+hp2+"\n"+name2+"'s SP:  "+p2Special+"\nChoose a Special!","Choose Your Command!",JOptionPane.INFORMATION_MESSAGE,null,special2,special2[0]);
			if(sChoice2.equals("Go Back"))
				attackP1(name1,name2,hp,atk2,def,def2,tempDef2,hp2);
			
		}
		return hp;
	}
	
}